<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>

<body>
    <body>
        <div id="main">
            <div class="wrapper">
                <form action="" method="post">
                    <div class="form-group">
                        <label class="form-label">Họ và tên</label>
                        <div class="form-input">
                            <input type="text" name="username" id="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Giới tính</label>
                        <div class="form-input">
                            <?php
                            $arsex = ["Nam", "Nữ"];
                            for ($i = 0; $i < 2; $i++) {
                                echo '  <div class="radio-group">
                                <input type="radio" id="' . $arsex[$i] . '"  name="arsex" value=" ' . $i . '">
                                <label class="radio-label" for="' . $arsex[$i] . '">' . $arsex[$i] . '</label>
                                        </div>';
                            }

                            ?>
                        </div>


                    </div>
                    <div class="form-group">
                        <label class="form-label">Phân khoa</label>
                        <div class="form-input">
                            <select name="khoa" id="pkhoa">
                                <?php
                                $arkhoa = ["", "MAT", "KDL"];
                                $phan_khoa = array(
                                    "MAT" => "Khoa học máy tính",
                                    "KDL" => "Khoa học vật liệu"
                                );
                                foreach ($arkhoa as $khoa) {
                                    echo ('<option value="' . $khoa . '">' . $phan_khoa[$khoa] . '</option>');
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="submit-btn">Đăng kí</button>
                </form>
            </div>
        </div>
    </body>

</html>